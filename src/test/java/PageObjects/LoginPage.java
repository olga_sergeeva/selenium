package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import Utils.BasePage;
import Utils.CustomWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Olga_Sergeeva1 on 2/1/2017.
 */


public class LoginPage extends BasePage {

    private WebElement emailField(){
        return new WebDriverWait(webDriver,10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='mailbox__login']")));
    };


    private WebElement passwordField(){
        return new WebDriverWait(webDriver,10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='mailbox__password']")));
    };

    private WebElement loginButton(){
        return new WebDriverWait(webDriver,10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='mailbox__auth__button']")));
    };

    private CustomWait customWait;

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
        customWait = new CustomWait(webDriver);
    }

    public LoginPage enterMailAddress(String login) {
        emailField().sendKeys(login);
        return this;
    }

    public LoginPage enterPassword(String password) {
        passwordField().sendKeys(password);
        return this;
    }

    public LoginPage clickNextButton() {
        loginButton().click();
        return this;
    }

    @Override
    public boolean isLoaded() {
        return customWait.isElementPresent(emailField()) && customWait.isElementPresent(loginButton());
    }

}

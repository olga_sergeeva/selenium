package PageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import Utils.BasePage;
import Utils.CustomWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Olga_Sergeeva1 on 2/1/2017.
 */
public class MailPage extends BasePage {

    private CustomWait customWait;

    public MailPage(WebDriver webDriver) {
        super(webDriver);
        customWait = new CustomWait(webDriver);
    }


    private WebElement writeButton() {
        return webDriver.findElement(By.xpath(".//a[@data-name='compose']"));
    }

    private WebElement nameField() {
        return new WebDriverWait(webDriver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(".//textarea[contains(@data-original-name, 'To')]")));
    }

    private WebElement topicField() {
        return new WebDriverWait(webDriver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@class = 'b-input']")));
    }

    private void bodyField(String body) {
        ((JavascriptExecutor)webDriver).executeScript("tinyMCE.activeEditor.setContent('" + body + "')");
    }

    private WebElement sendButton() {
        return webDriver.findElement(By.xpath(".//div[@data-name='send']"));
    }

    public MailPage clickWriteButton() {
        writeButton().click();
        return this;
    }

    public MailPage enterName(String name) {
        nameField().sendKeys(name);
        return this;
    }

    public MailPage enterTopic(String topic) {
        topicField().sendKeys(topic);
        return this;
    }

    public MailPage enterBody(String body) {
        bodyField(body);
        return this;
    }

    public MailPage clickSendButton() {
        sendButton().click();
        return this;
    }

//    @Override
//    public boolean isLoaded() {
//
//        return customWait.isElementPresent(writeButton);
//    }


    @Override
    public boolean isLoaded() {
        return customWait.isElementPresent(writeButton()) && customWait.isElementPresent(sendButton());
    }

}

package Utils;

import org.openqa.selenium.WebDriver;

/**
 * Created by Olga_Sergeeva1 on 2/1/2017.
 */
public abstract class BasePage {


        protected WebDriver webDriver;

        public BasePage(WebDriver webDriver){
            this.webDriver = webDriver;
        }

        public abstract boolean isLoaded();

        public BasePage loadPage(String url) {
            webDriver.get(url);
            return this;
        }
}

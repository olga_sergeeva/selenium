package Utils;

/**
 * Created by Olga_Sergeeva1 on 2/1/2017.
 */
public enum UrlProvider {

        GOOGLE_MAIN("https://mail.ru/");

        private final String baseUrl;

        private UrlProvider(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getUrl() {
            return baseUrl;
        }


}

package Tests;

import PageObjects.LoginPage;
import PageObjects.MailPage;
import Utils.UrlProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Olga_Sergeeva1 on 2/1/2017.
 */
public class MailCheck {


        private WebDriver webDriver;
        private String url;

        @BeforeMethod
        private void setUp() {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            webDriver = new ChromeDriver();
            url = UrlProvider.GOOGLE_MAIN.getUrl();
        }

        @AfterMethod
        private void tearDown() {
            if (webDriver != null) {
                webDriver.quit();
            }
        }

        @Test(dataProvider = "loginGMailData")
        public void shouldNotLoggedInTest(String login, String password, String name, String topic, String body) throws InterruptedException {
            webDriver.get(url);
            webDriver.manage().window().maximize();
            LoginPage loginPage = new LoginPage(webDriver);
            assertThat(loginPage.isLoaded()).isTrue();
            loginPage.enterMailAddress(login);
            loginPage.enterPassword(password);
            loginPage.clickNextButton();
            MailPage mailPage = new MailPage(webDriver);
      //    assertThat(mailPage.isLoaded()).isTrue();
            mailPage.clickWriteButton();
            mailPage.enterName(name);
            mailPage.enterTopic(topic);
            mailPage.enterBody(body);
            mailPage.clickSendButton();

        }

        @DataProvider(name = "loginGMailData")
        public Object[][] testData() {
            return new Object[][]{
                    {"test_automation_academy@mail.ru", "test_automation2017", "destiny.exe@mail.ru", "Introduction", "Hi"}
            };
        }

}
